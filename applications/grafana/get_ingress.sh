set -x
export INGRESS_IP=$(kubectl get service -n gitlab-managed-apps -o jsonpath='{.items[*].status.loadBalancer.ingress[0].ip}')
echo $INGRESS_IP > ingress_ip.env
echo $pwd
